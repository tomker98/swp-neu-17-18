public class InsertionSort
{
	public static void main(String[] args)
	{
		int[] a = {8, 16, 3, 3, 2, 5, -2};
		insertionSort(a);
		for(int i:a)
		{
			System.out.println(i);
		}
	}
	public static void insertionSort (int[] a)
	{
		for (int i = 1; i < a.length; i++)
		{
			int h = a[i];
			int j = i-1;
			while(j>=0 && a[j] > h)
			{
				a[j+1] = a[j];
				j--;
			}
			a[j+1]=h;
		}
	}
}