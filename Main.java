import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Main
{
	static String path;
	static ArrayList<String> fkfkArray = new ArrayList<String>();
	public static void main(String[] args) throws IOException
	{
		BufferedReader br = null;
		int counter = 0;
		Scanner s = new Scanner(System.in);
		int fc;
		
		
		try
		{
			FileInputStream fstream = new FileInputStream("C:/Users/Thomas/Desktop/test.txt");	
			br = new BufferedReader(new InputStreamReader(fstream));
			path = s.next();
			
		}
		catch(FileNotFoundException fnfe)
		{
			System.out.println("Datei nicht gefunden");
		}
		
		//C:\Users\Thomas\Desktop\test2.html
		
		String strLine;
		ArrayList<String> strArray = new ArrayList<String>();
		ArrayList<Integer> kursivArray = new ArrayList<Integer>();
		ArrayList<Integer> fettArray = new ArrayList<Integer>();
		ArrayList<Integer> kursivFettArray = new ArrayList<Integer>();
		ArrayList<String> headerArray = new ArrayList<String>();
		ArrayList<String> listArray = new ArrayList<String>();
		
		
		try
		{
			while ((strLine = br.readLine()) != null)
			{
				strArray.add(strLine);
				System.out.println (strLine);
			}
			System.out.println("");
			br.close();
		}
		catch(IOException ioe)
		{
			System.out.println("Datei konnte nicht eingelesen werden");
		}
		
		for(int i = 0; i < strArray.size(); i++)
		{
			String convertString = strArray.get(i);		
			int headerInt = convertString.indexOf(" ");
			if(!convertString.equals(""))
			{
				if((convertString.charAt(0) == '#'))
				{
					switch (headerInt)
					{
						case 1: convertString = convertString.replace("#", "<h1>");
						convertString = convertString + " </h1>";
						break;
						case 2: convertString = convertString.replace("##", "<h2>");
						convertString = convertString + " </h2>";
						break;
						case 3: convertString = convertString.replace("###", "<h3>");
						convertString = convertString + " </h3>";
						break;
						case 4: convertString = convertString.replace("####", "<h4>");
						convertString = convertString + " </h4>";
						break;
						case 5: convertString = convertString.replace("#####", "<h5>");
						convertString = convertString + " </h5>";
						break;
						case 6: convertString = convertString.replace("######", "<h6>");
						convertString = convertString + " </h6>";
						break;
					}
				}
			}
			headerArray.add(convertString);
		}
		
		for (int i = 0; i < headerArray.size(); i++)
		{
			String convertString = headerArray.get(i);
			
			if(!convertString.equals(""))
			{
				if((convertString.charAt(0) == '-') && (convertString.charAt(1) == ' ') && (counter == 0))
				{
					convertString = "<ol> <li>" + convertString.substring(1) + "</ol>";

					counter = 1;
				}
				else if((convertString.charAt(0) == '-') && (convertString.charAt(1) == ' ') && (counter == 1))
				{
					convertString = "<li>" + convertString.substring(1);
				}
				else if((convertString.charAt(0) != '-') && (counter == 1))
				{
					convertString += "</ol>";
				}
			}
			listArray.add(convertString);
		}
		
		for(int i = 0; i < listArray.size(); i++)
		{
			fc = 0;
			kursivFettArray.clear();
			fettArray.clear();
			kursivArray.clear();
			String convertString = listArray.get(i);
			if(!convertString.equals(""))
			{
				for(int j = 0; j < convertString.length(); j++)
				{
					if((convertString.charAt(j) == '*') && (convertString.charAt(j+1) == '*') && (convertString.charAt(j+2) == '*'))
					{
						kursivFettArray.add(j);
						j = j+2;
					}
					else if((convertString.charAt(j) == '*') && (convertString.charAt(j+1) == '*') && (convertString.charAt(j+2) != '*'))
					{
						fettArray.add(j);
						j = j+1;
					}
					else if((convertString.charAt(j) == '*') && (convertString.charAt(j+1) != '*'))
					{
						kursivArray.add(j);
					}
				}
				if(kursivFettArray.size() != 0)
				{
					for(int kf = 0; kf < kursivFettArray.size(); kf++)
					{
						if((kf == 0) || (kf % 2 == 0))
						{
							convertString = convertString.substring(0,kursivFettArray.get(kf)+fc) + "<b><i>" + convertString.substring(kursivFettArray.get(kf)+fc+3);
							fc+=3;
						}
						else if((kf >= 1) && (kf % 2 != 0))
						{
							convertString = convertString.substring(0, kursivFettArray.get(kf)+fc) + "</b></i>" + convertString.substring(kursivFettArray.get(kf)+fc+3);
							fc+=5;
						}
					}
				}
				if(fettArray.size() != 0)
				{
					for(int f = 0; f < fettArray.size(); f++)
					{
						if((f == 0) || (f % 2 == 0))
						{
							convertString = convertString.substring(0,fettArray.get(f)+fc) + "<b>" + convertString.substring(fettArray.get(f)+fc+2);
							fc+=1;
						}
						else if((f >= 1) && (f % 2 != 0))
						{
							convertString = convertString.substring(0, fettArray.get(f)+fc) + "</b>" + convertString.substring(fettArray.get(f)+fc+2);
							fc+=2;
						}
					}
				}
				if(kursivArray.size() != 0)
				{
					for(int k = 0; k < kursivArray.size(); k++)
					{
						if((k == 0) || (k % 2 == 0))
						{
							convertString = convertString.substring(0,kursivArray.get(k)+fc) + "<i>" + convertString.substring(kursivArray.get(k)+fc+1);
							fc+=2;
						}
						else if((k >= 1) && (k % 2 != 0))
						{
							convertString = convertString.substring(0,kursivArray.get(k)+fc) + "</i>" + convertString.substring(kursivArray.get(k)+fc+1);
							fc+=3;
						}
					}
				}
			}
			System.out.println(convertString);
			fkfkArray.add(convertString);
		}
		createFile(path);
	}
	static ArrayList<String> lines = new ArrayList<String>();

	public static void createFile(String path) throws IOException
	{
		try {
			FileWriter filewriter = new FileWriter(path);
			for (int i = 0; i < fkfkArray.size(); i++)
			{
				filewriter.write(fkfkArray.get(i));
				filewriter.append(System.getProperty("line.separator"));
			}
			filewriter.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	public boolean createFail(File fail)
	{
		if (fail != null)
		{
			try
			{
				fail.createNewFile();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			if (fail.isFile() && fail.canWrite() && fail.canRead())
			{
				return true;
			}
		}
		return false;
	}
}